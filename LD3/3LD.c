#include <avr/io.h>
#include <stdio.h>
#include <avr/interrupt.h>
#include <util/delay.h>

volatile uint8_t tot_overflow;

void timer2_init(void);

void main()
{
	DDRA = 0xFF;
	DDRD |= (1<<PD7);
	timer2_init();
	//sei();
	OCR2=0;
	int j=1;
	while(1)
	{
		if (tot_overflow >= 12) // Ar jau ivyko12 persipildymo pertraukciu?
		{
			// Ar laikmacio/skaitikio2 registras jau pasieke verte 53?
			if (TCNT2 >= 53)
			{
				for(int i=0; i<255; i++)
				{
					OCR2++;
					_delay_ms(100);
				}
				for(int i=0; i<255; i++)
				{
					OCR2--;
					_delay_ms(100);
				}				

				PORTA |= (j << 0); // Invertuoti itampos lygi PC0 i�vade
				j=j*2;
				//_delay_ms(1000);
				TCNT2 = 0; // Nunulinti Laikmacio/Skaitiklio2 registra
				tot_overflow = 0; // Nunulinti kintamaji, sauganti ivykusiu persipildymu pertraukciu skaiciu
			}
		}
	}
}
void timer2_init(void)
{
	TCCR2 |= (1<<WGM20)|(1<<WGM21); //select Fast PWM mode
	// Nustatyti dalikli (prescaler), lygu 256
	TCCR2 |=(1<<COM21)|(1<<CS20)|(1<<CS21)|(1<<CS22);
	// Inicializuoti laikmati/skaitikli2
	TCNT2 = 0;
	// Leisti persipildymo pertraukti Laikmaciui/Skaitikliui2
	TIMSK |= (1 << TOIE2);

	// Leisti globalias pertrauktis
	sei();
	tot_overflow = 0;
}
ISR(TIMER2_OVF_vect)
{
	tot_overflow++; 
}
